# angular-i18n

Angular 11 i18n example with ngx-translate/core

## Installation

```bash
npm install
```

## Usage

```bash
ng serve --open
```

## License
[GNU GPLv3](https://choosealicense.com/licenses/gpl-3.0/)